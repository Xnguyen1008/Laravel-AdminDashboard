<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Experience', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable($value=true);
            $table->string('jobs')->nullable($value=true);
            $table->string('certificate')->nullable($value=true);
            $table->string('time')->nullable($value=true);
            $table->string('describe')->nullable($value=true);
            $table->string('language')->nullable($value=true);
            $table->string('rating')->nullable($value=true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Experience');
    }
}
