<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonalInfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable($value=true);
            $table->string('describe')->nullable($value=true);
            // $table->string('firstName')->nullable($value=true);
            // $table->string('Lastname')->nullable($value=true);
            // $table->string('dateOfBirth')->nullable($value=true);
            // $table->string('nationality')->nullable($value=true);
            // $table->string('phone')->nullable($value=true);
            // $table->string('address')->nullable($value=true);
            // $table->string('freelance')->nullable($value=true);
            // $table->string('email')->nullable($value=true);
            // $table->string('spokenLanguage')->nullable($value=true);
            // $table->string('skype')->nullable($value=true);
            // $table->string('image')->nullable($value=true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonalInfo');
    }
}
