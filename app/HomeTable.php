<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeTable extends Model
{
    protected $table = 'hometable';
    protected $fillable = ['hello','name','job','textbg','firsttitle','lasttitle',
    'imageInfor'];
    public $timestamps = false;
}

    