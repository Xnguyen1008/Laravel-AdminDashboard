<?php

namespace App\Http\Controllers;

use App\HomeTable;
use view\testTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Symfony\Component\Debug\Tests\Fixtures\ToStringThrower;

class TableController extends Controller
{
    public function insert()
    {

        $insert = new HomeTable;
        $hello = $_POST["hello"];
        $name = $_POST["name"];
        $jobs = $_POST["jobs"];
        $textbg = $_POST["textbg"];
        $first = $_POST["first"];
        $last = $_POST["last"];
        $image = $_POST["image"];

        $insert->hello = "$hello";
        $insert->name = "$name";
        $insert->job = "$jobs";
        $insert->textbg = "$textbg";
        $insert->firsttitle = "$first";
        $insert->lasttitle = "$last";
        $insert->imageInfor = "$image";
        $insert->save();
        return redirect('admin');

    }

    public function update($id, $hello = null, $name = null, $job = null, $textbg = null, $firsttitle = null, $lasttitle = null, $imageInfor = null)
    {

        $item = HomeTable::find("$id");
        $item->hello = "$hello";
        $item->name = "$name";
        $item->job = "$job";
        $item->textbg = "$textbg";
        $item->firsttitle = "$firsttitle";
        $item->lasttitle = "$lasttitle";
        $item->imageInfor = "$imageInfor";
        $item->save();

        return redirect('admin');
    }
    public function delete($id)
    {
        HomeTable::destroy($id);
        return redirect('admin');
    }

    public function GetData()
    {
        $data = DB::table('hometable')->get()->toJson();
        print_r($data);
    }
}
