<?php

namespace App\Http\Controllers;

use App\PersonalInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Symfony\Component\Debug\Tests\Fixtures\ToStringThrower;

class PersonalInfoController extends Controller
{
    public function showtable(){  
        $data = DB::table('personalinfo')->get();
        // print_r($data);
        return view('personalinfotable')->with("data", $data);
    }


    // public function showtable(){
    //     return view('personalinfotable');
    // }
    public function insert()
    {

        $insert = new PersonalInfo;
        $title  = $_POST["title"];
        $describe   = $_POST["describe"];
        // $firstName   = $_GET["firstName"];
        // $lastname = $_GET["lastname"];  
        // $dateOfBirth  = $_GET["dateOfBirth"];
        // $nationality   = $_GET["nationality"];
        // $phone  = $_GET["phone"];
        // $address   = $_GET["address"];
        // $freelance = $_GET["freelance"];  
        // $email  = $_GET["email"];
        // $spokenLanguage   = $_GET["spokenLanguage"];
        // $skype  = $_GET["skype"];
        // $image  = $_GET["image"];

        $insert->title      = "$title";
        $insert->describe       = "$describe";
        // $insert->firstName        = "$firstName";
        // $insert->lastname     = "$lastname";
        // $insert->dateOfBirth = "$dateOfBirth";
        // $insert->nationality      = "$nationality";
        // $insert->phone       = "$phone";
        // $insert->address        = "$address";
        // $insert->freelance     = "$freelance";
        // $insert->email = "$email";
        // $insert->spokenLanguage  = "$spokenLanguage";
        // $insert->skype = "$skype";
        // $insert->image = "$image";
        $insert->save();
        return redirect(route('personal'));

    }
// /,$firstName= null,$lastname= null,$dateOfBirth= null,$nationality= null,$phone= null,$address= null,$freelance= null,$email= null,$spokenLanguage= null,$skype= null,$image= null
    public function update($id,$title= null,$describe= null)
    {

        $item = PersonalInfo::find("$id");
        $item-> title           ="$title";
        $item-> describe        ="$describe";
        // $item-> firstName       ="$firstName";
        // $item-> lastname        ="$lastname";
        // $item-> dateOfBirth     ="$dateOfBirth";
        // $item-> nationality     ="$nationality";
        // $item-> phone           ="$phone";
        // $item-> address         ="$address";
        // $item-> freelance       ="$freelance";
        // $item-> email           ="$email";
        // $item-> spokenLanguage  ="$spokenLanguage";
        // $item-> skype           ="$skype";
        // $item-> image           ="$image";
        $item->save();

        return redirect(route('personal'));   
    }
    public function delete($id)
    {
        PersonalInfo::destroy($id);
        return redirect(route('personal'));
    }

   
}
