<?php

namespace App\Http\Controllers;

use App\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Symfony\Component\Debug\Tests\Fixtures\ToStringThrower;

class ExperienceController extends Controller
{
    public function showtable(){
        return view('experiencetable');
    }
    public function insert()
    {

        $insert         = new Experience;
        $title          = $_POST["title"];
        $certificate    = $_POST["certificate"];
        $jobs           = $_POST["job"];
        $time           = $_POST["time"];
        $describe       = $_POST["describe"];
        $language       = $_POST["language"];
        $rating         = $_POST["rating"];

        $insert->title      = "$title";
        $insert->certificate= "$certificate";
        $insert->jobs       = "$jobs";
        $insert->time       = "$time";
        $insert->describe   = "$describe";
        $insert->language   = "$language";
        $insert->rating     = "$rating";
        $insert->save();
        return redirect('showtable-exp');

    }

    public function update($id,$title= null,$certificate= null,$jobs= null,$time= null,$describe= null,$language= null,$rating= null)
    {

        $item = Experience::find("$id");
        $item-> title       ="$title";
        $item-> certificate ="$certificate";
        $item-> jobs        ="$jobs";
        $item-> time        ="$time";
        $item-> describe    ="$describe";
        $item-> language    ="$language";
        $item-> rating      ="$rating";
        $item->save();

        return redirect('showtable-exp');   
    }
    public function delete($id)
    {
        Experience::destroy($id);
        return redirect('showtable-exp');
    }
    public function GetData(){
        $data = DB::table('experience')->get()->toJson();
        print_r($data);
    }
}
