<?php
 use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function(){
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::Post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/','AdminController@index')->name('admin.dashboard');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
});
Route::get('insert',function(){
        $admin = new App\Admin;

        $admin->username = "aaaaaa";
        $admin->email ="111@gmail.com" ;
        $admin->password = Hash::make('password') ;
        $admin->job_title = "train";

        $admin->save();
    
});




Route::post('insert','TableController@insert')->name('insert');

Route::get('updatetable','TableController@update');   
Route::get('delete/{id}', 'TableController@delete');
Route::get('update/{id}/{hello?}/{name?}/{job?}/{textbg?}/{first?}/{last?}/{image?}','TableController@update');
Route::get('table',function(){
    return  view('testTable');
});
Route::get('getdata','TableController@GetData');

Route::get('master',function(){
    return view('layouts.layout');
});
Route::get('Myprofile',function(){
    return view('myProfile');
});
Route::get('getMenu','AdminController@getmenu')->name('getmenu');
Route::get('test',function(){
        $menu= DB::table('hometable')->get();
        
        $value= array();
    for ($i=0; $i <count($menu) ; $i++) { 
    array_push($value,$menu[$i]->job);
    
   };
  
        echo '<pre>';
        print_r($value);
        echo '</pre>';

});


//route experience 
Route::get('showtable-exp','ExperienceController@showtable')->name('exp.show');
Route::post('insert-exp','ExperienceController@insert')->name('exp.insert');
Route::get('delete-exp/{id}', 'ExperienceController@delete')->name('exp.delete');
Route::get('update-exp/{id}/{title?}/{certificate?}/{job?}/{time?}/{describe?}/{language?}/{rating?}','ExperienceController@update')->name('exp.update');
Route::get('getdata-exp','ExperienceController@GetData');
//end route experience


//route personal 
Route::get('showtable-personal','PersonalInfoController@showtable')->name('personal');
Route::post('insert-personal','PersonalInfoController@insert')->name('personal.insert');
Route::get('delete-personal/{id}', 'PersonalInfoController@delete')->name('personal.delete');
Route::get('update-personal/{id}/{title?}/{certificate?}/{job?}/{time?}/{describe?}/{language?}/{rating?}','PersonalInfoController@update')->name('personal.update');
Route::get('getdata-personal','PersonalInfoController@GetData');
//end route personal