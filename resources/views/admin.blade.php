@extends('layouts.layout')
@section('head')
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>

<style>
    .titletalbe{
        float:left;
        padding-left :10px;
    }
</style>
        <script type="text/javascript">
        $(document).ready(function(){
            var request = $.get('/laravel/public/getdata/');
                request.done(function(response) {
                   
                   response = JSON.parse(response);
        
                   console.log(response);
                   for (index = 0; index < response.length; index++) {
                        $("#bodytable tr:last").after("<tr>"+
                        "<td><input type='text'  readonly id ='txtID_"+ response[index].id +"' value="+ response[index].id +" /></td>"+
                        "<td><input type='text'   id ='txtHello_"+ response[index].id +"' value='"+ response[index].hello +"'/></td>"+
                        "<td><input type='text'   id ='txtName_"+ response[index].id +"' value='"+ response[index].name +"'/></td>"+
                        "<td><input type='text'   id ='txtJob_" + response[index].id + "' value='" + response[index].job + "'/></td>"+
                        "<td><input type='text'   id ='txtTextbg_"+ response[index].id +"' value='"+ response[index].textbg +"'/></td>"+
                        "<td><input type='text'   id ='txtFirstTitle_"+ response[index].id +"' value='"+ response[index].firsttitle +"'/></td>"+
                        "<td><input type='text'   id ='txtLastTitle_"+ response[index].id +"' value='"+ response[index].lasttitle +"'/></td>"+
                        "<td><input type='text'   id ='txtImage_"+ response[index].id +"' value='"+ response[index].imageInfor +"'/></td>"+
                        "<td> <input type='button' value='update' onclick='UpdateData("+response[index].id+")'/></td>"+
                        "<td><a href='http://localhost/laravel/public/delete/"+response[index].id+" '>Delete</a></td>"+
                        "</tr>");
                    }
                });
           
        });
        
        </script>
@endsection
@section('main')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Table test</h4>
                        <p class="category">Here is a subtitle for this table</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table  id ="table"class="table table-hover table-striped">
                            <thead>
                                
                                    <th>    ID          </th>
                                    <th>    Hello       </th>
                                    <th>    Name        </th>
                                    <th>    Jobs        </th>
                                    <th>    Textbg      </th>
                                    <th>    FirstTitle  </th>  
                                    <th>    LastTitle   </th>  
                                    <th>    ImageInfor  </th>  
                                    <th></th>  
                                    <th></th>     
                            
                            </thead>
                           <tbody id = "bodytable">
                                <tr>
                                    
                                </tr>
                           </tbody>
                         
                            <form method="post" action="insert">
                                @csrf
                                    <div style ="padding-left:10px  " >
                                        <b>Insert</b>:<br>

                                      <div class="titletalbe"  >
                                            HELLO:<br>
                                            <input type="text" name="hello" ><br>
                                            NAME:<br>
                                            <input type="text" name="name" ><br>
                                        </div>

                                        <div class ="titletalbe">
                                            JOBS:<br>
                                            <input type="text" name="jobs" ><br>
                                            TEXTBG:<br>
                                            <input type="text" name="textbg" ><br>
                                        </div>

                                        <div class="titletalbe">
                                            FIRSTTITLE:<br>
                                            <input type="text" name="first" ><br>
                                            LASTTITLE:<br>
                                            <input type="text" name="last" ><br>
                                        
                                        </div>

                                        <div class="titletalbe" >
                                            IMAGEINFOR:<br>
                                            <input type="text" name="image" ><br>
                                           
                                        </div>
                                   
                                    <br>
                                    </div>
                                    <input type ="submit" action ="{{route('insert')}}"  value= "Insert">
                         
                        </table>

                    </div>
                </div>
            </div>
                     <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="header">
                                <h4 class="title">Table Info</h4>
                                <p class="category">Here is a subtitle for this table</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table2" class="table table-hover">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Tex1</th>
                                    	<th>Tex2</th>
                                    	<th>Tex3</th>
                                    	<th>Tex4</th>
                                    </thead>
                                    <tbody>
                                        
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                         </div>
                     </div>
                 
             
         </div>
     </div>
 </div>
 
{{--     
    <table id ="table">
            <tr>
                <th>ID</th>
                <th>Name</th>   
                <th>Age</th>
                <th></th>
                <th></th>
            </tr>
    </table> --}}
    
    
    
    {{-- <div style ="float :left">
    <b>Update</b> :<br>
    id:<br>
    <input type="text" id="txtID" readonly><br>
    name:<br>
    <input  type="text" id="txtName"><br>
    age:<br>
    <input  type="text" id="txtAge"><br>
    <input  id = "update"  type ="button"  value= "Update" onclick="UpdateData()"  ><br>
    </div>
    
    <div style = "float:left">
    Search:<br>
    <input  type="text" id="txtSearch"><br>
    <input type="button" value="search" onclick="Search()">
    </div> --}}
    </form>
    <script type="text/javascript">
       
        
        function UpdateData(id) {
            

            var IntID  = $("#txtID_"+id).val();
            var Hello  = $("#txtHello_"+id).val();
            var Name   = $("#txtName_"+id).val();
            var Job    = $("#txtJob_"+id).val();
            var TextBg = $("#txtTextbg_"+id).val();
            var First  = $("#txtFirstTitle_"+id).val();
            var Last   = $("#txtLastTitle_"+id).val();
            var Image  = $("#txtImage_"+id).val();

            // var data  = {
            //     id          : IntID,
            //     hello       : Hello,
            //     name        : Name,
            //     job         : Job,
            //     textbg      : TextBg,
            //     firsttile   : First,
            //     lasttile    : Last,
            //     imageInfo   : Image,
            //  };
     
            var request= $.get('/laravel/public/update/'+IntID+'/'+Hello+'/'+Name+'/'+Job+'/'+TextBg+'/'+First+'/'+Last+'/'+Image);
                request.done(function(){
                    window.location="http://localhost/laravel/public/admin";
                });
        
        };
     
        // function Search() {
        //     var txtSearch =$("#txtSearch").val();
        //     var request= $.get('/trainning/public/Search/'+txtSearch);
        //     request.done(function(rsp){
                
    
                // response = JSON.parse(rsp);
                // for (index = 0; index < response.length; index++) {
                //     $("#table tr:last").after("<tr><td>" + response[index].id + "</td>"+
                //     "<td>" +response[index].name+ "</td>"+
                //     "<td>" +response[index].age+ "</td>"+
                //     "</tr>");
                // }
        //     });
        // };
        
    
    </script>
@endsection