<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Lina - Creative vCard, Resume, CV</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.animatedheadline.css" />
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/skins/yellow.css" />

    <!-- Template JS Files -->
    <script src="js/modernizr.custom.js"></script>

</head>

<body class="dark">
    <!-- Preloader Start -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- Preloader Ends -->
    <!-- Wrapper Starts -->
    <div class="wrapper">
        <div id="bl-main" class="bl-main">
            <!-- Top Left Section Starts -->
            {{--
            <section class="topleft">
                <div class="bl-box row valign-wrapper">
                    <div class="row valign-wrapper">
                        <div class="title-heading">
                            <div class="selector uppercase" id="selector">
                                <h3 class="ah-headline p-none m-none">
                                    <span class="font-weight-300">Hi There ! I'm</span>
                                    <span class="ah-words-wrapper">
										<b class="is-visible">lina marzouki</b>
										<b>a web designer</b>
										<b>a photographer</b>
										<b>a blogger</b>
									</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}
            <!-- Top Left Section Ends -->

            <?php
    $experience= DB::table('experience')->get();           
    $menu = DB::table('hometable')->get();
    $personal = DB::table('personalinfo')->get();
    $jobs = array();
    for ($i=0; $i <count($menu) ; $i++) { 
        array_push($jobs,$menu[$i]->job);
        
    }
  ?>
                @for ($i = 0; $i< count($menu); $i++) @if ($i===0 ) 
                <section class="topleft">
                    <div class="bl-box row valign-wrapper">
                        <div class="row valign-wrapper">
                            <div class="title-heading">
                                <div class="selector uppercase" id="selector">
                                    <h3 class="ah-headline p-none m-none">
                                        <span class="font-weight-300">{{$menu[$i]->hello}}</span>
                                        <span class="ah-words-wrapper">
                                        <b class="is-visible">{{$menu[$i]->name}} </b>
                                    @for ($j = 0; $j < count($jobs) ; $j++)
                                        <b>{{$menu[$j]->job}}</b>
                                    @endfor
                                </span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                    @else
                    <section>
                        <!-- About Title Starts -->
                        <div class="bl-box valign-wrapper">
                            <div class="page-title center-align">
                                <span class="title-bg">{{$menu[$i]->textbg}}</span>
                                <h2 class="center-align">
                                    <span data-hover="{{$menu[$i]->firsttitle}}">{{$menu[$i]->firsttitle}}</span>
                                    <span data-hover="{{$menu[$i]->lasttitle}}">{{$menu[$i]->lasttitle}}</span>
                                </h2>
                            </div>
                        </div>

                        <div class="bl-content">

                            @switch($menu[$i]->textbg) @case("Resume")
                            <!-- Main Heading Starts -->
                            <div class="container page-title center-align">
                                <h2 class="center-align">
                                    <span data-hover="{{$menu[$i]->firsttitle}}">{{$menu[$i]->firsttitle}} </span>
                                    <span data-hover="{{$menu[$i]->lasttitle}}">{{$menu[$i]->lasttitle}}</span>
                                </h2>
                                <span class="title-bg">{{$menu[$i]->textbg}}</span>
                            </div>
                            <!-- Main Heading Ends -->
                            <div class="container infos">
                                <!-- Divider Starts -->
                                <div class="divider center-align">
                                    <span class="outer-line"></span>
                                    <span class="fa fa-vcard" aria-hidden="true"></span>
                                    <span class="outer-line"></span>
                                </div>
                                <!-- Divider Ends -->
                                <!-- Personal Informations Starts -->
                                <div class="row">
                                    <!-- Picture Starts -->
                                    <div class="col s12 m5 l4 xl3 profile-picture">
                                        <img src="images/photo-about.jpg" class="responsive-img my-picture" alt="My Photo">
                                    </div>
                                    <!-- Picture Ends -->
                                    @for($k = 0; $k< count($personal); $k++) 
                                    @if ($k===0) 
                                    <div class="col s12 m7 l8 xl9 personal-info">
                                        <h6 class="uppercase"><i class="fa fa-user"></i>{{$personal[$k]->title}}</h6>
                                        <div class="col m12 l7 xl7 p-none">
                                            <p class="second-font">{{$personal[$k]->describe}}</p>
                                        </div>
                                        @else @if(($k+1)< (count($personal)/2)) <div class="col s12 m12 l6 p-none">
                                            <ul class="second-font list-1">
                                                <li><span class="font-weight-600">{{$personal[$k]->title}} : </span>{{$personal[$k]->describe}}</li>
                                            </ul>
                                    </div>
                                    @else
                                <div class="col s12 m12 l6 p-none">
                                    <ul class="second-font list-2">
                                        <li><span class="font-weight-600">{{$personal[$k]->title}}: </span>{{$personal[$k]->describe}}</li>
                                    </ul>
                                </div>
                                @endif @endif @endfor
                                <a href="#" class="col s12 m12 l4 xl4 waves-effect waves-light btn font-weight-500">
                                Download Resume <i class="fa fa-file-pdf-o"></i>
                                </a>
                                <a href="blog-light.html" class="col s12 m12 l4 xl4 btn btn-blog font-weight-500">
                                My Blog <i class="fa fa-edit"></i>
                            </a>
                            </div>
                        </div></
        </div>
        <!-- Personal Informations Ends -->
        <!-- Resume Starts -->
        <div class="resume-container">
            <div class="container">
                <div class="valign-wrapper row"> --}}
                    <!-- Resume Menu Starts -->

                    <div class="resume-list col l4">
                        @for ($n = 0; $n <count($experience) ; $n++) 
                        <div class="resume-list-item" data-index="{{$n}}" id="resume-list-item-{{$n}}">
                            <div class="resume-list-item-inner">
                                <h6 class="resume-list-item-title uppercase"><i class="fa fa-briefcase">{{$experience[$n]->title}}</i></h6>
                            </div>
                        </div>
                    @endfor
                </div>
                <!-- Resume Menu Ends -->
                <!-- Resume Content Starts -->
                <div class="col s12 m12 l8 resume-cards-container">
                    <div class="resume-cards">
                    <!-- Experience Starts -->
                    @for ($m = 0; $m < count($experience); $m++)
                    <div class="resume-card resume-card-{{$m}}" data-index="{{$m}}">
                            <!-- Experience Header Title Starts -->
                            <div class="resume-card-header">
                                <div class="resume-card-name"><i class="fa fa-briefcase"></i> {{$experience[$m]->title}}</div>
                            </div>
                            <!-- Experience Header Title Ends -->
                            <!-- Experience Content Starts -->
                           
                            <div class="resume-card-body {{$experience[$m]->title}}">
                               
                                <div class="resume-card-body-container second-font">
                                    <!-- Single Experience Starts -->
                                    @if($experience[$m]->title=="Experience")
                                    <div class="resume-content">
                                        <h6 class="uppercase"><span> {{$experience[$m]->jobs}} </span></h6>
                                        <span class="date"><i class="fa fa-calendar-o"></i> {{$experience[$m]->time}}</span>
                                        <p>{{$experience[$m]->describe}}</p>
                                    </div>
                                    <!-- Single Experience Ends -->
                                    <span class="separator"></span>

                                    <!-- Single Experience Ends -->
                                    @elseif($experience[$m]->title=="Education") --}}
                                    @endif
                                </div>
                             

                                
                            </div>
     
                           <!-- Experience Content Starts -->
                        </div>
                
                        @endfor
                        <div class="container">
                            <div class="valign-wrapper row">
                                <!-- Resume Menu Starts -->
                                
                                <div class="resume-list col l4">
                                    
                                    <div class="resume-list-item is-active" data-index="0" id="resume-list-item-0">
                                        <div class="resume-list-item-inner">
                                            <h6 class="resume-list-item-title uppercase"><i class="fa fa-briefcase"></i> Experience</h6>
                                        </div>
                                    </div>
                                    <div class="resume-list-item" data-index="1" id="resume-list-item-1">
                                        <div class="resume-list-item-inner">
                                            <h6 class="resume-list-item-title uppercase"><i class="fa fa-graduation-cap"></i> Education</h6>
                                        </div>
                                    </div>
                                    <div class="resume-list-item" data-index="2" id="resume-list-item-2">
                                        <div class="resume-list-item-inner">
                                            <h6 class="resume-list-item-title uppercase"><i class="fa fa-star"></i> Skills</h6>
                                        </div>
                                    </div>
                                </div>
								<!-- Resume Menu Ends -->
								<!-- Resume Content Starts -->
                                <div class="col s12 m12 l8 resume-cards-container">
                                    <div class="resume-cards">
										<!-- Experience Starts -->
                                        <div class="resume-card resume-card-0" data-index="0">
											<!-- Experience Header Title Starts -->
                                            <div class="resume-card-header">
                                                <div class="resume-card-name"><i class="fa fa-briefcase"></i> Experience</div>
                                            </div>
											<!-- Experience Header Title Ends -->
											<!-- Experience Content Starts -->
                                            <div class="resume-card-body experience">
                                                <div class="resume-card-body-container second-font">
													<!-- Single Experience Starts -->
                                                    <div class="resume-content">
														<h6 class="uppercase"><span>Web Designer - </span>Envato</h6>
														<span class="date"><i class="fa fa-calendar-o"></i> 2015 - 2018</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium orci sit amet mi ullamcorper</p>
                                                    </div>
													<!-- Single Experience Ends -->
                                                    <span class="separator"></span>
													<!-- Single Experience Starts -->
                                                    <div class="resume-content">
														<h6 class="uppercase"><span>Web Developer - </span>Google</h6>
														<span class="date"><i class="fa fa-calendar-o"></i> 2011 - 2015</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium orci sit amet mi ullamcorper</p>
                                                    </div>
													<!-- Single Experience Ends -->
                                                    <span class="separator"></span>
													<!-- Single Experience Starts -->
                                                    <div class="resume-content">
														<h6 class="uppercase"><span>Community Manager - </span>Adobe</h6>
														<span class="date"><i class="fa fa-calendar-o"></i> 2007 - 2011</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium orci sit amet mi ullamcorper</p>
                                                    </div>
													<!-- Single Experience Ends -->
                                                </div>
                                            </div>
											<!-- Experience Content Starts -->
                                        </div>
										<!-- Experience Ends -->
										<!-- Education Starts -->
                                        <div class="resume-card resume-card-1" data-index="1">
											<!-- Education Header Title Starts -->
                                            <div class="resume-card-header">
                                                <div class="resume-card-name"><i class="fa fa-graduation-cap"></i> Education</div>
                                            </div>
											<!-- Education Header Title Starts -->
                                            <div class="resume-card-body education">
                                                <div class="resume-card-body-container second-font">
													<!-- Single Education Starts -->
                                                    <div class="resume-content">
														<h6 class="uppercase"><span>Engineering Diploma - </span>Oxford University</h6>
														<span class="date"><i class="fa fa-calendar-o"></i> 2015 - 2018</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium orci sit amet mi ullamcorper</p>
                                                    </div>
													<!-- Single Education Ends -->
                                                    <span class="separator"></span>
													<!-- Single Education Starts -->
                                                    <div class="resume-content">
														<h6 class="uppercase"><span>Masters Degree - </span>Paris University</h6>
														<span class="date"><i class="fa fa-calendar-o"></i> 2011 - 2015</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium orci sit amet mi ullamcorper</p>
                                                    </div>
													<!-- Single Education Ends -->
                                                    <span class="separator"></span>
													<!-- Single Education Starts -->
                                                    <div class="resume-content">
														<h6 class="uppercase"><span>Bachelor Degree - </span>Berlin Higher Institute</h6>
														<span class="date"><i class="fa fa-calendar-o"></i> 2007 - 2011</span>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium orci sit amet mi ullamcorper</p>
                                                    </div>
													<!-- Single Education Ends -->
                                                </div>
                                            </div>
                                        </div>
										<!-- Education Ends -->
										<!-- Skills Starts -->
                                        <div class="resume-card resume-card-2" data-index="2">
											<!-- Skills Header Title Starts -->
                                            <div class="resume-card-header">
                                                <div class="resume-card-name"><i class="fa fa-star"></i> Skills</div>
                                            </div>
											<!-- Skills Header Title Starts -->
                                            <div class="resume-card-body skills">
                                                <div class="resume-card-body-container second-font">
                                                    <div class="row">
														<!-- Skills Row Starts -->
                                                        <div class="col s6">
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">html</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">javascript</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-empty"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">css</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-empty"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">php</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">jquery</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">angular js</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
                                                        </div>
														<!-- Skills Row Ends -->
														<!-- Skills Row Starts -->
                                                        <div class="col s6">
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">wordpress</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-empty"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">joomla</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-empty"></i> <i class="fa fa-star-o"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">magento</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">drupal</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">Adobe Photoshop</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
															<!-- Single Skills Starts -->
                                                            <div class="resume-content">
																<h6 class="uppercase">Adobe illustrator</h6>
																<p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-empty"></i> <i class="fa fa-star-o"></i></p>
                                                            </div>
															<!-- Single Skills Ends -->
                                                        </div>
														<!-- Skills Row Ends -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										<!-- Skills Ends -->
                                    </div>
                                </div>
								<!-- Resume Content Ends -->
                            </div>
                        </div>
                    </div>
    @break
        @case("works")
            <h1>my Portfolio</h1>
        @break
        @case("CONTACT")
        <h1>Get in touch</h1>
        @break
    @default
     @endswitch
    </section>
    @endif @endfor

    <!-- Portfolio Panel Items Starts -->
    <div class="bl-panel-items" id="bl-panel-work-items">
        <!-- Project Starts -->
        <div data-panel="panel-1">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <img class="responsive-img" src="images/projects/project-1.jpg" alt="project" />
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Image Project</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">Envato</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">php, html, css, javascript</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Project Starts -->
        <div data-panel="panel-2">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <div class="carousel carousel-slider">
                        <a class="carousel-item" href="#one!"><img class="responsive-img" src="images/projects/project-2.jpg" alt="project" ></a>
                        <a class="carousel-item" href="#two!"><img class="responsive-img" src="images/projects/project-1.jpg" alt="project" ></a>
                        <a class="carousel-item" href="#three!"><img class="responsive-img" src="images/projects/project-3.jpg" alt="project" ></a>
                        <a class="carousel-item" href="#four!"><img class="responsive-img" src="images/projects/project-4.jpg" alt="project" ></a>
                    </div>
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Slider Project</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">Themeforest</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">php, html, css, javascript</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Project Starts -->
        <div data-panel="panel-3">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <div class="videocontainer">
                        <iframe class="youtube-video" src="https://www.youtube.com/embed/7e90gBu4pas?enablejsapi=1&version=3&playerapiid=ytplayer"
                            allowfullscreen></iframe>
                    </div>
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Youtube Video</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">Photodune</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">Adobe After Effects, Adobe Audition</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Project Starts -->
        <div data-panel="panel-4">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <video id="video" class="responsive-video" controls poster="images/projects/video/video-poster.png">
								<source src="images/projects/video/video.mp4" type="video/mp4">
							</video>
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Local Video</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">Videohive</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">Adobe After Effects, Movie Maker</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Project Starts -->
        <div data-panel="panel-5">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <img src="images/projects/project-5.jpg" alt="project" />
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Image Project</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">Graphicriver</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">Adobe Photoshop, Gimp</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Project Starts -->
        <div data-panel="panel-6">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <img src="images/projects/project-6.jpg" alt="project" />
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Image Project</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">Activeden</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">Adobe Flash, Paint</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Project Starts -->
        <div data-panel="panel-7">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <img src="images/projects/project-7.jpg" alt="project" />
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Image Project</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">3D Ocean</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">3DS Max, Adobe Photoshop</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Project Starts -->
        <div data-panel="panel-8">
            <div class="row">
                <!-- Project Main Content Starts -->
                <div class="col s12 l6 xl6">
                    <img src="images/projects/project-8.jpg" alt="project" />
                </div>
                <!-- Project Main Content Ends -->
                <!-- Project Details Starts -->
                <div class="col s12 l6 xl6">
                    <h3 class="font-weight-600 white-text uppercase">Image Project</h3>
                    <ul class="project-details white-text second-font">
                        <li><i class="fa fa-user"></i><span class="font-weight-600"> Client </span>: <span class="font-weight-400 uppercase">Audiojungle</span></li>
                        <li><i class="fa fa-calendar"></i><span class="font-weight-600"> Date </span>: <span class="font-weight-400 uppercase">02/11/2017</span></li>
                        <li><i class="fa fa-cogs"></i> <span class="font-weight-600"> Used Technologies</span> : <span class="font-weight-400 uppercase">Adobe Audition, Adobe Premiere</span></li>
                    </ul>
                    <hr>
                    <p class="white-text second-font">Occupy american apparel freegan cliche. Mustache trust fund 8-bit jean shorts mumblecore thundercats.
                        Pour-over small batch forage cray, banjo post-ironic flannel keffiyeh cred ethnic semiotics next
                        level tousled fashion axe. Sustainable cardigan keytar fap bushwick bespoke.</p>
                    <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a>
                </div>
                <!-- Project Details Ends -->
            </div>
        </div>
        <!-- Project Ends -->
        <!-- Portfolio Navigation Starts -->
        <nav>
            <!-- Previous Work Icon Starts -->
            <span class="control-button bl-previous-work uppercase font-weight-700"><i class="fa fa-chevron-left"></i></span>
            <!-- Previous Work Icon Ends -->
            <!-- Close Work Icon Starts -->
            <span class="control-button fa fa-close fa-2x bl-icon-close center-align" id="bl-icon-close"></span>
            <!-- Close Work Icon Ends -->
            <!-- Next Work Icon Starts -->
            <span class="control-button bl-next-work uppercase font-weight-700"><i class="fa fa-chevron-right"></i></span>
            <!-- Previous Work Icon Ends -->
        </nav>
        <!-- Portfolio Navigation Ends -->
    </div>
    <!-- Portfolio Panel Items Ends -->
    </div>
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/jquery.animatedheadline.js"></script>
    <script src="js/boxlayout.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/jquery.hoverdir.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>