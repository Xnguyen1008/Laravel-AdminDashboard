@extends('layouts.layout')
@section('head')
<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>

<style>
    .titletalbe{
        float:left;
        padding-left :10px;
    }
</style>
        <script type="text/javascript">
        $(document).ready(function(){
            var request = $.get('/laravel/public/getdata-exp/');
                request.done(function(response) {
                   
                   response = JSON.parse(response);
        
                   console.log(response);
                   for (index = 0; index < response.length; index++) {
                        $("#bodytable tr:last").after("<tr>"+
                        "<td><input type='text'  readonly id ='txtID_"+ response[index].id +"' value="+ response[index].id +" /></td>"+
                        "<td><input type='text'   id ='txtTitle_"+ response[index].id +"' value='"+ response[index].title +"'/></td>"+
                        "<td><input type='text'   id ='txtCertificate_"+ response[index].id +"' value='"+ response[index].certificate +"'/></td>"+
                        "<td><input type='text'   id ='txtJobs_" + response[index].id + "' value='" + response[index].jobs + "'/></td>"+
                        "<td><input type='text'   id ='txtTime_"+ response[index].id +"' value='"+ response[index].time +"'/></td>"+
                        "<td><input type='text'   id ='txtDescribe_"+ response[index].id +"' value='"+ response[index].describe +"'/></td>"+
                        "<td><input type='text'   id ='txtLanguage_"+ response[index].id +"' value='"+ response[index].language +"'/></td>"+
                        "<td><input type='text'   id ='txtRating_"+ response[index].id +"' value='"+ response[index].rating +"'/></td>"+
                        "<td> <input type='button' value='update' onclick='UpdateData("+response[index].id+")'/></td>"+
                        "<td><a href='http://localhost/laravel/public/delete-exp/"+response[index].id+" '>Delete</a></td>"+
                        "</tr>");
                    }
                });
           
        });
        
        </script>
@endsection
@section('main')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Table test</h4>
                        <p class="category">Here is a subtitle for this table</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table  id ="table"class="table table-hover table-striped">
                            <thead>
                                
                                    <th>    ID          </th>
                                    <th>    Title       </th>
                                    <th>    Certificate </th>
                                    <th>    Jobs        </th>
                                    <th>    Time        </th>
                                    <th>    Describe  </th>  
                                    <th>    Language   </th>  
                                    <th>    Rating  </th>  
                                    <th></th>  
                                    <th></th>     
                            
                            </thead>
                           <tbody id = "bodytable">
                                <tr>
                                    
                                </tr>
                           </tbody>
                         
                            <form method="post" action="insert-exp">
                                @csrf
                                    <div style ="padding-left:10px  " >
                                        <b>Insert</b>:<br>

                                      <div class="titletalbe"  >
                                            Title:<br>
                                            <input type="text" name="title" ><br>
                                            Certificate:<br>
                                            <input type="text" name="certificate" ><br>
                                        </div>

                                        <div class ="titletalbe">
                                            JOBS:<br>
                                            <input type="text" name="job" ><br>
                                            Time:<br>
                                            <input type="text" name="time" ><br>
                                        </div>

                                        <div class="titletalbe">
                                            Describe:<br>
                                            <input type="text" name="describe" ><br>
                                            Language:<br>
                                            <input type="text" name="language" ><br>
                                        
                                        </div>

                                        <div class="titletalbe" >
                                            Rating:<br>
                                            <input type="text" name="rating" ><br>
                                           
                                        </div>
                                   
                                    <br>
                                    </div>
                                    <input type ="submit" action ="{{route('exp.insert')}}"  value= "Insert">
                         
                        </table>

                    </div>
                </div>
            </div>
         </div>
     </div>
 </div>

    </form>
    <script type="text/javascript">
       
        
        function UpdateData(id) {
            

            var IntID  = $("#txtID_"+id).val();
            var Title  = $("#txtTitle_"+id).val();
            var Certificate  = $("#txtCertificate_"+id).val();
            var Job    = $("#txtJobs_"+id).val();
            var Time = $("#txtTime_"+id).val();
            var Describe  = $("#txtDescribe_"+id).val();
            var Language   = $("#txtLanguage_"+id).val();
            var Rating  = $("#txtRating_"+id).val();
            
            
            var request= $.get('/laravel/public/update/'+IntID+'/'+Title+'/'+Certificate+'/'+Job+'/'+Time+'/'+Describe+'/'+Language+'/'+Rating);
                request.done(function(){
                    window.location="http://localhost/laravel/public/admin";
                });
        
        };
     
    </script>
@endsection