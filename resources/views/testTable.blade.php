<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?php use App\Http\Controllers\TableController; ?>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>



    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th,
        td {
            padding: 15px;
        }

        #id,
        #age,
            {
            width: 10px;
        }

        #name {
            width: 400px;
        }

        div {
            margin: 20px;
        }

        #edit {
            width: 40px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
    var request = $.get('/trainning/public/getdata/');
        request.done(function(response) {
           
           response = JSON.parse(response);

           console.log(response);
           for (index = 0; index < response.length; index++) {
                $("#table tr:last").after("<tr>"+
                "<td><input type='text'  readonly id ='txtID_"+ response[index].id +"' value="+ response[index].id +"></td>"+
                "<td><input type='text'   id ='txtName_"+ response[index].id +"' value="+ response[index].name +"></td>"+
                "<td><input type='text'   id ='txtAge_"+ response[index].id +"' value="+ response[index].age +"></td>"+
                "<td> <input type='button' value='update' onclick='UpdateData("+response[index].id+")'></td>"+
                "<td><a href='http://localhost/trainning/public/delete/"+response[index].id+" '>Delete</a></td>"+
                "</tr>");
            }
        });
   
});

    </script>
</head>

<body>

    <table id="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Age</th>
            <th></th>
            <th></th>
        </tr>
    </table>

    <form method="post" action="showTable">
        <div style="float:left ">
            <b>Insert</b>:<br> name:

            <br>
            <input type="text" name="iname"><br> age:
            <br>
            <input type="text" name="iage"><br>

            <input type="submit" action="insert" value="Insert">
            <br>
        </div>

        <div style="float :left">
            <b>Update</b> :<br> id:
            <br>
            <input type="text" id="txtID" readonly><br> name:
            <br>
            <input type="text" id="txtName"><br> age:
            <br>
            <input type="text" id="txtAge"><br>
            <input id="update" type="button" value="Update" onclick="UpdateData()"><br>
        </div>

        <div style="float:left">
            Search:<br>
            <input type="text" id="txtSearch"><br>
            <input type="button" value="search" onclick="Search()">
        </div>
    </form>
    <script>
        function UpdateData(id) {
        var IntID = $("#txtID_"+id).val();
        var Name = $("#txtName_"+id).val();
        var Age = $("#txtAge_"+id).val();
        var data = {
            id : IntID,
            name : Name,
            age : Age,
        };
        var request= $.get('/trainning/public/update/'+IntID+'/'+Name+'/'+Age);
            request.done(function(){
                window.location="http://localhost/trainning/public/table";
            });
    
    };
    function Search() {
        var txtSearch =$("#txtSearch").val();
        var request= $.get('/trainning/public/Search/'+txtSearch);
        request.done(function(rsp){
            

            // response = JSON.parse(rsp);
            // for (index = 0; index < response.length; index++) {
            //     $("#table tr:last").after("<tr><td>" + response[index].id + "</td>"+
            //     "<td>" +response[index].name+ "</td>"+
            //     "<td>" +response[index].age+ "</td>"+
            //     "</tr>");
            // }
        });
    };

    </script>
</body>

</html>